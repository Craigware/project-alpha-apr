from django.urls import path
from projects.views import ListProjects, ProjectDetails, CreateProject

urlpatterns = [
    path("", ListProjects, name="list_projects"),
    path("<int:id>/", ProjectDetails, name="show_project"),
    path("create/", CreateProject, name="create_project"),
]
