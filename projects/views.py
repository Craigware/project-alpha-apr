from django.shortcuts import render, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectCreationForm


@login_required
def ListProjects(request):
    projects = Project.objects.filter(owner=request.user)

    context = {
        "projects": projects,
    }

    return render(request, "project_list.html", context)


@login_required
def ProjectDetails(request, id):
    project = Project.objects.get(id=id)

    context = {
        "project": project,
    }

    return render(request, "project_details.html", context)


@login_required
def CreateProject(request):
    if request.method == "POST":
        form = ProjectCreationForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data["name"]
            description = form.cleaned_data["description"]
            owner = form.cleaned_data["owner"]

            Project.objects.create(
                name=name,
                description=description,
                owner=owner,
            )

            return redirect("list_projects")
    else:
        form = ProjectCreationForm()

    context = {
        "form": form,
    }

    return render(request, "project_creation.html", context)
