from django.urls import path
from tasks.views import CreateTask, AssignedTasks

urlpatterns = [
    path("create/", CreateTask, name="create_task"),
    path("mine/", AssignedTasks, name="show_my_tasks"),
]
