from django.shortcuts import render, redirect
from tasks.forms import TaskForm
from tasks.models import Task
from django.contrib.auth.decorators import login_required


@login_required
def CreateTask(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data["name"]
            start_date = form.cleaned_data["start_date"]
            due_date = form.cleaned_data["due_date"]
            project = form.cleaned_data["project"]
            assignee = form.cleaned_data["assignee"]

            Task.objects.create(
                name=name,
                start_date=start_date,
                due_date=due_date,
                project=project,
                assignee=assignee,
            )

            return redirect("list_projects")

    else:
        form = TaskForm()

    context = {
        "form": form,
    }

    return render(request, "task_creation.html", context)


@login_required
def AssignedTasks(request):
    tasks = Task.objects.filter(assignee=request.user)

    context = {
        "tasks": tasks,
    }

    return render(request, "assigned_tasks.html", context)
